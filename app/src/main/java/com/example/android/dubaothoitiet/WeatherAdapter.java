package com.example.android.dubaothoitiet;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.dubaothoitiet.Entities.WeatherForecast;

import java.util.ArrayList;

/**
 * Created by vietphuong on 11/15/17.
 */

public class WeatherAdapter extends BaseAdapter {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<WeatherForecast> data;

    public WeatherAdapter(Context context, ArrayList<WeatherForecast> data) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = layoutInflater.inflate(R.layout.weather_list_item, viewGroup, false);
        WeatherForecast weatherForecast = (WeatherForecast) getItem(i);

        TextView date = (TextView) row.findViewById(R.id.date_tv);
        date.setText(weatherForecast.getDate());

        TextView temp = (TextView) row.findViewById(R.id.temp_tv);
        temp.setText(weatherForecast.getTemp());

        TextView description = (TextView) row.findViewById(R.id.description_tv);
        description.setText(weatherForecast.getDescription());

        AppCompatImageView forecastImage = (AppCompatImageView) row.findViewById(R.id.forecast_image_view);
        forecastImage.setImageDrawable(weatherForecast.getForecastImage(context));

        return row;
    }
}
