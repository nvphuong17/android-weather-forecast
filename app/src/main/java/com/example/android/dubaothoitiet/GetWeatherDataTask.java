package com.example.android.dubaothoitiet;

import android.os.AsyncTask;
import android.util.Log;

import com.example.android.dubaothoitiet.Entities.WeatherForecast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by vietphuong on 11/16/17.
 */

public class GetWeatherDataTask extends AsyncTask<String, Void, String> {
    GetDataCompleteListener getDataCompleteListener;

    public GetWeatherDataTask(GetDataCompleteListener getDataCompleteListener) {
        this.getDataCompleteListener = getDataCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        getDataCompleteListener.startDownloadData();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            return downloadData(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        Log.d("Diachi", s);
        ArrayList<WeatherForecast> weatherForecasts = new ArrayList<>();

        try {
            JsonObject jsonData = new Gson().fromJson(s, JsonObject.class);
            JsonArray dailyForecasts = jsonData.getAsJsonArray("DailyForecasts");

            for (Integer i = 0; i < 5; i++) {
                JsonObject forecastData = dailyForecasts.get(i).getAsJsonObject();
                weatherForecasts.add(
                    new WeatherForecast(forecastData)
                );
            }

            getDataCompleteListener.getComplete(weatherForecasts);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String downloadData(String urlString) throws IOException {
        InputStream inputStream = null;
        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            inputStream = connection.getInputStream();
            return convertToString(inputStream);
        } finally {
            if (inputStream != null)
                inputStream.close();
        }
    }

    private String convertToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
            builder.append(line);
        return new String(builder);
    }
}
