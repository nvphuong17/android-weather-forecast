package com.example.android.dubaothoitiet.Entities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.android.dubaothoitiet.R;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by vietphuong on 11/15/17.
 */

public class WeatherForecast implements Serializable {
    private Date date;
    private Temperature temperature;
    private Temperature realFeelTemperature;
    private Phase day;
    private Phase night;

    private final String unit = "°C";

    public WeatherForecast(JsonObject jsonObject) {
        date = new Date(1000 * Long.parseLong(jsonObject.getAsJsonPrimitive("EpochDate").toString()));
        temperature = new Temperature(jsonObject.getAsJsonObject("Temperature"));
        realFeelTemperature = new Temperature(jsonObject.getAsJsonObject("RealFeelTemperature"));
        day = new Phase(jsonObject.getAsJsonObject("Day"));
        night = new Phase(jsonObject.getAsJsonObject("Night"));
    }

    public String getDescription() {
        return day.getIconPhrase();
    }

    public String getTemp() {
        DecimalFormat df = new DecimalFormat("0.#");
        return df.format(temperature.getMinValue()) + " - " + df.format(temperature.getMaxValue()) + " " + unit;
    }

    public Drawable getForecastImage(Context context) {
        int image, icon = day.getIcon();
        if (1 <= icon && icon <= 5)
            image = R.drawable.ic_sun;
        else if (6 <= icon && icon <= 8)
            image = R.drawable.ic_cloud;
        else if (icon == 11)
            image = R.drawable.ic_fog;
        else if (12 <= icon && icon <= 18)
            image = R.drawable.ic_rain;
        else
            image = R.drawable.ic_launcher_foreground;
        return context.getResources().getDrawable(image);
    }

    public String getDate() {
        return new SimpleDateFormat("E MMM d, yyyy").format(date);
    }

    public Phase getDay() {
        return day;
    }

    public Phase getNight() {
        return night;
    }
}
