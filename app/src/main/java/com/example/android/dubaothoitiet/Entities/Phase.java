package com.example.android.dubaothoitiet.Entities;

import com.google.gson.JsonObject;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by vietphuong on 11/18/17.
 */

public class Phase implements Serializable {
    private Integer icon;
    private String iconPhrase;
    private Double precipitationProbability;
    private Double thunderstormProbability;
    private Double rainProbability;

    public Phase(Integer icon, String iconPhrase, Double precipitationProbability, Double thunderstormProbability, Double rainProbability) {
        this.icon = icon;
        this.iconPhrase = iconPhrase;
        this.precipitationProbability = precipitationProbability;
        this.thunderstormProbability = thunderstormProbability;
        this.rainProbability = rainProbability;
    }

    public Phase(JsonObject jsonObject) {
        icon = jsonObject.get("Icon").getAsInt();
        iconPhrase = jsonObject.get("IconPhrase").getAsString();
        precipitationProbability = jsonObject.get("PrecipitationProbability").getAsDouble();
        thunderstormProbability = jsonObject.get("ThunderstormProbability").getAsDouble();
        rainProbability = jsonObject.get("RainProbability").getAsDouble();
    }

    public Integer getIcon() {
        return icon;
    }

    public String getIconPhrase() {
        return iconPhrase;
    }

    public Double getPrecipitationProbability() {
        return precipitationProbability;
    }

    public Double getThunderstormProbability() {
        return thunderstormProbability;
    }

    public Double getRainProbability() {
        return rainProbability;
    }

    public String getDetail() {
        String detail = "";
        DecimalFormat df = new DecimalFormat("0.#");
        detail += "Xác suất lượng mưa: " + df.format(getPrecipitationProbability()) + "%\n";
        detail += "Xác suất giông: " + df.format(getThunderstormProbability()) + "%\n";
        detail += "Xác suất có mưa: " + df.format(getRainProbability()) + "%";
        return detail;
    }
}
