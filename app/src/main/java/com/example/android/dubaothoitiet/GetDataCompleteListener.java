package com.example.android.dubaothoitiet;

import com.example.android.dubaothoitiet.Entities.WeatherForecast;

import java.util.ArrayList;

/**
 * Created by vietphuong on 11/16/17.
 */

public interface GetDataCompleteListener {
    public void startDownloadData();
    public void getComplete(ArrayList<WeatherForecast> weatherForecasts);
}
