package com.example.android.dubaothoitiet.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.widget.TextView;

import com.example.android.dubaothoitiet.R;
import com.example.android.dubaothoitiet.Entities.WeatherForecast;

import org.w3c.dom.Text;

public class DetailForecastActivity extends AppCompatActivity {
    TextView tempTextView;
    TextView dateTextView;
    TextView dayDetailTextView;
    TextView nightDetailTextView;
    AppCompatImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_forecast);

        dateTextView = (TextView) findViewById(R.id.detail_date_tv);
        tempTextView = (TextView) findViewById(R.id.detail_temp_tv);
        dayDetailTextView = (TextView) findViewById(R.id.day_detail_tv);
        nightDetailTextView = (TextView) findViewById(R.id.night_detail_tv);
        imageView = (AppCompatImageView) findViewById(R.id.forecast_detail_image_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            WeatherForecast weatherForecast = (WeatherForecast) getIntent().getSerializableExtra("forecast");

            dateTextView.setText(weatherForecast.getDate());
            tempTextView.setText(weatherForecast.getTemp());
            dayDetailTextView.setText(weatherForecast.getDay().getDetail());
            nightDetailTextView.setText(weatherForecast.getNight().getDetail());
            imageView.setImageDrawable(weatherForecast.getForecastImage(this));
        }
    }
}
