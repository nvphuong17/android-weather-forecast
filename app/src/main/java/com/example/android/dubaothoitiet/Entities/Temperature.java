package com.example.android.dubaothoitiet.Entities;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by vietphuong on 11/18/17.
 */

public class Temperature implements Serializable {
    private double minValue;
    private double maxValue;

    public Temperature(Double minValue, Double maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Temperature(JsonObject jsonObject) {
        minValue = jsonObject.getAsJsonObject("Minimum").get("Value").getAsDouble();
        maxValue = jsonObject.getAsJsonObject("Maximum").get("Value").getAsDouble();
    }

    public Double getMinValue() {
        return minValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }
}
