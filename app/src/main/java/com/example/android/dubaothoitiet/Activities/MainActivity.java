package com.example.android.dubaothoitiet.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.android.dubaothoitiet.GetDataCompleteListener;
import com.example.android.dubaothoitiet.GetWeatherDataTask;
import com.example.android.dubaothoitiet.R;
import com.example.android.dubaothoitiet.WeatherAdapter;
import com.example.android.dubaothoitiet.Entities.WeatherForecast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements GetDataCompleteListener {
    ListView weatherListView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherListView = (ListView) findViewById(R.id.weather_list_view);
        weatherListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            WeatherForecast weatherForecast = (WeatherForecast) adapterView.getAdapter().getItem(i);
            Intent intent = new Intent(MainActivity.this, DetailForecastActivity.class);
            intent.putExtra("forecast", weatherForecast);

            if (intent.resolveActivity(getPackageManager()) != null)
                startActivity(intent);
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.loading_indicator);

        fetchData();
    }

    public void fetchData() {
        startDownloadData();
        String requestUrl = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/353412?apikey=HG17wrwgRoTToXCA5iy9Pws9tC5SeIHg&language=vi-vn&metric=true&details=true";
        new GetWeatherDataTask(this).execute(requestUrl);
    }

    @Override
    public void startDownloadData() {
        weatherListView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void getComplete(ArrayList<WeatherForecast> weatherForecasts) {
        WeatherAdapter adapter = new WeatherAdapter(this, weatherForecasts);
        weatherListView.setAdapter(adapter);

        weatherListView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh_action) {
            fetchData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
